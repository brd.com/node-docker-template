const server = require('./cluster')

server.start({
  dev: false,
  workers: process.env.WORKERS || 2,
  workerMaxRequests: 1000, // gracefully restart worker after 1000 requests
  workerMaxLifetime: 3600, // gracefully restart worker after 1 hour
  address: process.env.HOST || '0.0.0.0',
  port: process.env.PORT || 3000,
})
