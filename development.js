const app = require('express')();

app.use(require('./server'));

app.listen(process.env.PORT,process.env.HOST || '0.0.0.0',function(err) {
  if(err) {
    console.error(err);
    process.exit(1);
  }
  console.log("Listening on ",process.env.PORT,process.env.VIRTUAL_HOST);
});
